var assert = require('assert');
var crypto = require('crypto');
var tenantFactory = require('..').tenantFactory;
var fixtures = require('./fixtures');

describe('ac hipchat tenant factory', function () {

  it('should combine an installable payload and capabilities document into a full tenant profile', function *() {
    var installable = fixtures.load('tenant-installable.json');
    var capabilities = fixtures.load('tenant-capabilities.json');
    var webhookToken = crypto.createHash('sha1')
      .update(installable.oauthId)
      .update(installable.oauthSecret)
      .digest('hex');
    assert.deepEqual(tenantFactory(installable, capabilities), {
      id: installable.oauthId,
      secret: installable.oauthSecret,
      group: installable.groupId,
      room: installable.roomId,
      webhookToken: webhookToken,
      links: {
        capabilities: capabilities.links.self,
        base: capabilities.links.homepage,
        api: capabilities.links.api,
        token: capabilities.capabilities.oauth2Provider.tokenUrl
      }
    });
  });

});
