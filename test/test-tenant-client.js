var assert = require('assert');
var MockHttpClient = require('./mock-http-client');
var MemoryStore = require('ac-node').MemoryStore;
var RestClient = require('..').RestClient;
var tenantFactory = require('..').tenantFactory;
var fixtures = require('./fixtures');
var wait = require('co-wait');

describe('ac hipchat tenant client', function () {

  var store = MemoryStore();
  var installablePayload = fixtures.load('tenant-installable.json');
  var capabilities = fixtures.load('tenant-capabilities.json');
  var tenant = tenantFactory(installablePayload, capabilities);
  var cache = store.narrow(tenant.id).narrow('token');
  var httpClient;
  var restClient;

  beforeEach(function *() {
    httpClient = MockHttpClient(61);
    restClient = RestClient(httpClient).forTenant(tenant, cache, [
      'admin_group',
      'admin_room',
      'manage_rooms',
      'send_message',
      'send_notification',
      'view_group',
      'view_messages'
    ]);
  });

  afterEach(function *() {
    yield store.clear();
  });

  if (!/\bfast\b/.test(process.env.NODE_TEST)) {
    it('should get tokens, and refresh them when they expire', function *() {
      var token = yield restClient.getToken('send_notification');
      assert.equal(token, '1a2b3c4d5e6f');
      yield wait(100);
      token = yield restClient.getToken('send_notification');
      assert.equal(token, '1a2b3c4d5e6f');
      yield wait(1000);
      token = yield restClient.getToken('send_notification');
      assert.equal(token, '2b3c4d5e6f7g');
    });
  }

  it('should get all emoticons', function *() {
    yield restClient.getEmoticons();
  });

  it('should get one emoticon', function *() {
    yield restClient.getEmoticon('foo');
  });

  it('should delete an oauth session', function *() {
    yield restClient.deleteSession('1a2b3c4d5e6f');
  });

  it('should get an oauth session', function *() {
    yield restClient.getSession('1a2b3c4d5e6f');
  });

  it('should get a room message', function *() {
    yield restClient.getRoomMessage('room-id', 'message-id');
  });

  it('should create a room', function *() {
    yield restClient.createRoom({
      name: 'foo',
      topic: 'bar',
      guestAccess: true,
      ownerUserId: 'user-id',
      privacy: 'public'
    });
  });

  it('should get all rooms', function *() {
    yield restClient.getRooms();
  });

  it('should get recent room history', function *() {
    yield restClient.getRecentRoomHistory('foo');
  });

  it('should send a room notification', function *() {
    yield restClient.sendNotification(1, 'This is a test message');
  });

  it('should update a room', function *() {
    yield restClient.updateRoom('foo', {
      name: 'foo',
      privacy: 'public',
      is_archived: false,
      is_guest_accessible: true,
      topic: 'bar',
      owner: {
        id: 'user-id'
      }
    });
  });

  it('should get a room', function *() {
    yield restClient.getRoom('foo');
  });

  it('should delete a room', function *() {
    yield restClient.deleteRoom('foo');
  });

  it('should create a room webhook', function *() {
    yield restClient.createRoomWebhook(1, {event: 'room_enter', name: 'abcdef1234567890', url: 'https://example.com/webhook'});
    // deprecated
    yield restClient.createWebhook(1, {event: 'room_enter', name: 'abcdef1234567890', url: 'https://example.com/webhook'});
  });

  it('should get all room webhooks', function *() {
    yield restClient.getRoomWebhooks('foo');
    // deprecated
    yield restClient.getWebhooks('foo');
  });

  it('should get room statistics', function *() {
    yield restClient.getRoomStatistics('foo');
  });

  it('should get room members', function *() {
    yield restClient.getRoomMembers('foo');
  });

  it('should reply to a message', function *() {
    yield restClient.replyToMessage('foo', 'parentId', 'message');
  });

  it('should get room members', function *() {
    yield restClient.getRoomMembers('foo');
  });

  it('should set a room topic', function *() {
    yield restClient.setRoomTopic('foo', 'topic');
  });

  it('should share a link with a room', function *() {
    yield restClient.shareLinkWithRoom('foo', 'link', 'message');
  });

  it('should add a room member', function *() {
    yield restClient.addRoomMember('foo', 'user-id');
  });

  it('should remove a room member', function *() {
    yield restClient.removeRoomMember('foo', 'user-id');
  });

  it('should delete a room webhook', function *() {
    yield restClient.deleteRoomWebhook('foo', 1234);
    // deprecated
    yield restClient.deleteWebhook('foo', 1234);
  });

  it('should get a room webhook', function *() {
    yield restClient.getRoomWebhook('foo', 1234);
  });

  it('should get room history', function *() {
    yield restClient.getRoomHistory('foo');
  });

  it('should get a private chat message', function *() {
    yield restClient.getPrivateChatMessage('foo', 'message-id');
  });

  it('should get recent private chat history', function *() {
    yield restClient.getRecentPrivateChatHistory('foo');
  });

  it('should update a user photo', function *() {
    yield restClient.updateUserPhoto('user-id', 'base64');
  });

  it('should delete a user photo', function *() {
    yield restClient.deleteUserPhoto('user-id');
  });

  it('should update a user', function *() {
    yield restClient.updateUser('user-id', {
      name: 'foo',
      title: 'tester',
      presence: {},
      mention_name: 'foo',
      is_group_admin: true,
      timezone: 'mst',
      password: 'password',
      email: 'foo@bar.com'
    });
  });

  it('should delete a user', function *() {
    yield restClient.deleteUser('user-id');
  });

  it('should get a user', function *() {
    yield restClient.getUser('user-id');
  });

  it('should create a user', function *() {
    yield restClient.createUser({
      name: 'user-id',
      title: 'title',
      mention_name: 'mention-name',
      is_group_admin: true,
      timezone: 'mst',
      password: 'password',
      email: 'user-id@foo.com'
    });
  });

  it('should get all users', function *() {
    yield restClient.getUsers();
  });

  it('should share a link with a user', function *() {
    yield restClient.shareLinkWithUser('user-id', 'link', 'message');
  });

});
