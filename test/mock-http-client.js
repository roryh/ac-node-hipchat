var assert = require('assert');
var mockHttpClient = require('ac-node/test/mock-http-client');
var fixtures = require('./fixtures');

module.exports = function (tokenExpiresIn) {
  var apiUrl = 'https://mock.hipchat.com/v2';
  var mockClient = mockHttpClient();
  var capabilities = fixtures.load('tenant-capabilities.json');
  var token1 = '1a2b3c4d5e6f';
  var token2 = '2b3c4d5e6f7g';
  var token;
  var tokenType = 'client_credentials';
  var groupId = 1;
  var groupName = 'Test Group';

  mockClient.mount('GET', apiUrl + '/capabilities', function (url, options, callback) {
    assert.ok(!options.body);
    assert.ok(options.json);
    // TODO: request assertions
    var body = capabilities;
    var response = {
      statusCode: 200,
      body: capabilities
    };
    callback(null, response, body);
  });

  mockClient.mount('GET', apiUrl + '/emoticon', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/emoticon/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/oauth/token/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/oauth/token', function (url, options, callback) {
    token = token === token1 ? token2 : token1;
    assert.ok(options.json);
    assert.ok(options.body);
    assert.equal(options.body.grant_type, tokenType);
    assert.equal(typeof options.body.scope, 'string');
    assert.ok(options.auth);
    assert.equal(typeof options.auth.username, 'string');
    assert.equal(typeof options.auth.password, 'string');
    var body = {
      access_token: token,
      expires_in: tokenExpiresIn,
      token_type: tokenType,
      group_id: groupId,
      group_name: groupName,
      scope: options.body.scope
    };
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response, body);
  });

  mockClient.mount('GET', apiUrl + '/oauth/token/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*/history/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/room', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 201,
      body: options.body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*/history/latest', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/room/*/notification', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.body);
    assert.ok(options.body.message);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/room/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/room/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/room/*/webhook', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.body);
    assert.ok(options.body.event);
    assert.ok(options.body.name);
    assert.ok(options.body.url);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {
      id: 123,
      links: {
        self: 'options.body.url'
      }
    };
    var response = {
      statusCode: 201,
      body: body
    };
    callback(null, response, body);
  });

  mockClient.mount('GET', apiUrl + '/room/*/webhook', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response, body);
  });

  mockClient.mount('GET', apiUrl + '/room/*/statistics', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/room/*/reply', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*/member', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/room/*/topic', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/room/*/share/link', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/room/*/member/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 204,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/room/*/member/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/room/*/webhook/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*/webhook/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/room/*/history', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/user/*/history/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/user/*/photo', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/user/*/photo', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/user/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('DELETE', apiUrl + '/user/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/user/*', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('POST', apiUrl + '/user', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 201,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('GET', apiUrl + '/user', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var body = {/* no need for a mock response body in this test */};
    var response = {
      statusCode: 200,
      body: body
    };
    callback(null, response);
  });

  mockClient.mount('PUT', apiUrl + '/user/*/share/link', function (url, options, callback) {
    assert.ok(options.json);
    assert.ok(options.auth);
    assert.equal(options.auth.bearer, token1);
    var response = {
      statusCode: 204
    };
    callback(null, response);
  });

  mockClient.notFound(function (url, options, callback) {
    var body = {error: {code: '404', message: 'No handler found for mock request route ' + this.route}};
    var response = {
      statusCode: 404,
      headers: {'Content-Type': 'application/json'},
      body: body
    };
    callback(null, response, body);
  });

  return mockClient;
};
