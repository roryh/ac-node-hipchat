var _ = require('lodash');
var check = require('check-types');
var verify = check.verify;
var wh = require('./webhooks');
var param = encodeURIComponent;

function WebhookManager(parent, store, tenant, client, baseUrl, webhookPath) {
  if (!(this instanceof WebhookManager)) {
    return new WebhookManager(parent, store, tenant, client, baseUrl, webhookPath);
  }
  this._parent = parent;
  this._store = store;
  this._tenant = tenant;
  this._client = client;
  this._baseUrl = baseUrl;
  this._webhookPath = webhookPath;
}

// name
// roomId, name
WebhookManager.prototype.get = function (roomId, name) {
  var self = this;
  if (typeof roomId === 'string') {
    name = roomId;
    roomId = self._tenant.room;
    if (!roomId) {
      throw new Error('Room id not found in method arguments or tenant');
    }
  }
  verify.string(name);
  return self._store.get(name).then(function (definition) {
    if (definition) {
      return definition;
    }
    // ask the parent manager if it has a globally registered webhook with the desired name
    return self._parent.get(name);
  });
};

// event
// 'room_message', pattern
// definition
// roomId, event
// roomId, 'room_message', pattern
// roomId, definition
WebhookManager.prototype.add = function () {
  var self = this;
  var args = [].slice.call(arguments);
  var roomId, event, pattern;
  if (typeof args[0] === 'number') {
    roomId = args[0];
    args.shift();
  } else {
    roomId = self._tenant.room;
  }
  if (args[0] != null && typeof args[0] === 'object') {
    event = args[0].event;
    pattern = args[0].pattern;
  } else {
    event = args[0];
    pattern = args[1];
  }
  if (!roomId) {
    throw new Error('Room id not found in method arguments or tenant');
  }
  verify.string(event);
  var definition = {event: event};
  if (pattern) {
    definition.pattern = pattern;
  }
  definition = wh.normalize(definition, self._baseUrl, self._webhookPath, self._tenant.webhookToken);
  verify.string(definition.name);
  return self._client.createWebhook(roomId, definition).then(function (webhook) {
    definition.id = webhook.id;
    return self._store.set(definition.name, definition).then(function () {
      return definition;
    });
  });
};

// name
// roomId, name
WebhookManager.prototype.remove = function (roomId, name) {
  var self = this;
  if (typeof roomId === 'string') {
    name = roomId;
    roomId = self._tenant.room;
    if (!roomId) {
      throw new Error('Room id not found in method arguments or tenant');
    }
  }
  verify.string(name);
  return self._store.get(name).then(function (definition) {
    if (definition) {
      return self._client.deleteWebhook(roomId, definition.id).then(function () {
        return self._store.del(name);
      });
    }
  });
};

module.exports = WebhookManager;
