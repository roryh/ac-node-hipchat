var check = require('check-types');
var verify = check.verify;
var resolve = require('url').resolve;
var clone = require('clone');
var wh = require('./webhooks');

var transforms = [
  metas,
  links,
  scopes,
  installable,
  configurable,
  webhooks
];

module.exports = function (descriptor, options, urls) {
  verify.object(descriptor.capabilities);
  verify.webUrl(urls.base);
  descriptor = clone(descriptor);
  transforms.forEach(function (transform) {
    descriptor = transform(descriptor, options, urls);
  });
  return descriptor;
};

module.exports.use = function (transform) {
  transforms.push(transform);
};

function metas(descriptor, options, urls) {
  if (!descriptor.key && options.name) {
    descriptor.key = options.name;
  }
  if (!descriptor.name && options.displayName) {
    descriptor.name = options.displayName;
  }
  if (!descriptor.description && options.description) {
    descriptor.description = options.description;
  }
  if (!descriptor.version && options.version) {
    descriptor.version = options.version;
  }
  var vendor;
  if ((!descriptor.vendor || !descriptor.vendor.name) && options.author) {
    vendor = descriptor.vendor = descriptor.vendor || {};
    if (typeof options.author === 'string') {
      vendor.name = options.author;
    } else if (options.author.name) {
      vendor.name = options.author.name;
    }
  }
  if ((!descriptor.vendor || !descriptor.vendor.url) && options.author && options.author.url) {
    vendor = descriptor.vendor = descriptor.vendor || {};
    vendor.url = options.author.url;
  }
  return descriptor;
}

function links(descriptor, options, urls) {
  var links = descriptor.links = descriptor.links || {};
  links.self = ensureUrl(links.self, urls.base, urls.descriptor);
  links.homepage = ensureUrl(links.homepage, urls.base, urls.homepage);
  return descriptor;
}

function scopes(descriptor, options, urls) {
  if (!descriptor.capabilities.hipchatApiConsumer) {
    descriptor.capabilities.hipchatApiConsumer = {scopes: []};
  }
  return descriptor;
}

function installable(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  var installable = capabilities.installable = capabilities.installable || {};
  if (!installable.allowGlobal) {
    installable.allowGlobal = false;
  }
  if (!installable.allowRoom) {
    installable.allowRoom = false;
  }
  installable.callbackUrl = ensureUrl(installable.callbackUrl, urls.base, urls.installable);
  return descriptor;
}

function configurable(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  var configurable = capabilities.configurable;
  if (configurable) {
    if (!configurable.url || !check.string(configurable.url)) {
      delete capabilities.configurable;
    } else if (!check.webUrl(capabilities.configurable.url)) {
      configurable.url = buildUrl(urls.base, configurable.url);
    }
  }
  return descriptor;
}

function webhooks(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.webhook)) {
    capabilities.webhook = capabilities.webhook.map(function (webhook) {
      return wh.normalize(webhook, urls.base, urls.webhook);
    });
  }
  return descriptor;
}

function ensureUrl(value, base, def) {
  if (!value && def) {
    value = buildUrl(base, def);
  } else if (check.string(value) && !check.webUrl(value)) {
    value = buildUrl(base, value);
  }
  return value;
}

function buildUrl(base, path) {
  return (base + '/' + path).replace(/([^:])\/{2,3}/g, '$1/');
}
