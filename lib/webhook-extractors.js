exports.room_enter = function (webhook) {
  var item = webhook.item;
  return {
    webhookId: webhook.webhook_id,
    room: item.room,
    sender: item.sender
  };
};

exports.room_exit = function (webhook) {
  var item = webhook.item;
  return {
    webhookId: webhook.webhook_id,
    room: item.room,
    sender: item.sender
  };
};

exports.room_message = function (webhook) {
  var item = webhook.item;
  return {
    webhookId: webhook.webhook_id,
    room: item.room,
    sender: item.message.from,
    message: item.message,
    content: item.message.message
  };
};

exports.room_notification = function (webhook) {
  var item = webhook.item;
  return {
    webhookId: webhook.webhook_id,
    room: item.room,
    sender: {
      name: item.message.from
    },
    message: item.message,
    content: item.message.message
  };
};

exports.room_topic_change = function (webhook) {
  var item = webhook.item;
  return {
    webhookId: webhook.webhook_id,
    room: item.room,
    sender: item.sender,
    topic: item.topic
  };
};
